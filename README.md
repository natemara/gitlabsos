# gitlabsos

gitlabsos provides a unified method of gathering info and logs from GitLab
and the system it's running on.

This script works best if run on a machine _while_ an issue is occurring or minutes
after. But don't fret if you miss your window of opportunity. Run gitlabsos
anyway and it will still gather a wealth of helpful information for
identifying issues.

**NOTE**: Please consider this script to be in an Alpha state. This means:

- don't depend on it always collecting the info it should
- the script is subject to being rewritten at any time and probably will be
- I've made reasonable efforts to avoid it, but it could damage your system (see LICENSE for warranty disclaimer)

## Limitations

- **Only works on Omnibus installs**. This means that the script won't collect
    GitLab-related information for k8s chart deployments or source installs.
- The resulting archive _could_ end up being large. In an attempt to reduce the
    log and archive size, gitlabsos only grabs the last 10MB of each log file.

## Get started

The gitlabsos program is a simple ruby script, and you can place it anywhere you like
and execute it however you see fit. I recommend putting it at `/usr/local/sbin/gitlabsos.rb`.

```sh
cp gitlabsos.rb /usr/local/sbin/gitlabsos.rb
chmod 700 /usr/local/sbin/gitlabsos.rb
# this will actually execute the script
gitlabsos.rb
```